﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuguController : MonoBehaviour {

    private int i, j;
    public Text str_gugu;

    // Use this for initialization
    void Start()
    {
        this.str_gugu.text = "구구단 표시준비...";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void gugudan()
    {
        this.str_gugu.text = "";

        for (i = 2; i < 10; i++)
        {
            for (j = 1; j < 10; j++)
            {
                str_gugu.text = str_gugu.text + i.ToString() + " * " + j.ToString() + " = " + (i * j).ToString() + "    ";
            }

            str_gugu.text = str_gugu.text + "\n";
        }
    }
}
